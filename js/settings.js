// Writer : Mehmet Ozgur Bayhan
// Date : 2016/03/19
/**
 * Main settings for imaheuploader
 * @type {{thumb_w: number, max_h: number, resize_list: number[][], thumb_h: number, max_w: number, upload_url: string, maxImageCount: number, cell_html: string, redirect_url: string}}
 */
var MOBSettings = {
    'maxImageCount': 5,// maximum image count in process, also upload file limit
    'thumb_w': 120, // thumbnail width and height
    'thumb_h': 120,
    'max_w': 1024, // maximum image size reduced before upload
    'max_h': 768,
    'redirect_url': "Default.aspx", // redirect url after upload
	'upload_url': "FileUploadHandler.ashx", // upload url
	'cell_html': '<div id="{container_id}" class="container_cells"><table><tr><td colspan="4"><canvas id="{canvas_id}" width="{canvas_width}" height="{canvas_height}"></canvas></td></tr><tr><td><button id="{btn_edit_id}" type="button" name="button" onclick="{fn_edit}"><span class="ui-icon   ui-icon-scissors"></span></button></td><td><button id="{btn_rotate_left_id}" type="button" name="button" onclick="{fn_rotate_left}"><span class="ui-icon  ui-icon-arrowthick-1-w"></span></button></td><td><button id="{btn_rotate_right_id}" type="button" name="button" onclick="{fn_rotate_right}"><span class="ui-icon  ui-icon-arrowthick-1-e"></span></button></td><td><button id="{btn_del_id}" type="button" name="button" onclick="{fn_delete}"><span class="ui-icon  ui-icon-close"></span></button></td></tr><tr><td colspan="4"><input id="{input_tag_id}" type="text" style="width:{canvas_width}px;"/></td></tr></table></div>'
};
