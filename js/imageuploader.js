// Writer : Mehmet Ozgur Bayhan
// Date : 2016/03/19
var UPLOAD_QUERY_ITEM = -1;
var TOTAL_UPLOAD_ITEM = -1;
var MOBImageFactory = {
    'active_image_id': null,
    'free_image_id': 0,
    'image_list': [],
    'cx': 0,
    'cy': 0,
    'cv': 0,
    'ch': 0,
    'fn_add_image_to_list': function(img_id, img_obj, img_size){
        /**
         * Adds an image to the list
         * @type {{img_id: *, img_size: *, img_obj: *}}
         */
        var mobimg = {
            "img_id": img_id,
            "img_obj": img_obj,
            "img_size": img_size
        };
        this.image_list.push(mobimg);
    },
    'fn_get_image_obj_by_id': function(img_id){
        /**
         * returns image object by id from list
         * @type {null}
         */
        var retval = null;
        for(var i = 0; i < this.image_list.length; i++){
            if(this.image_list[i].img_id === img_id){
                retval = this.image_list[i].img_obj;
                break
            }
        }
        return retval;
    },
    'fn_delete_image_by_id': function(img_id){
        /**
         * deletes image by id from list
         */
        for(var i = 0; i < this.image_list.length; i++){
            if(this.image_list[i].img_id === img_id){
                this.image_list.splice(i, 1);
                break;
            }
        }
    },
    'fn_update_image_by_id': function(img_id, img_object){
        /**
         * updates image object by id
         */
        for(var i = 0; i < this.image_list.length; i++){
            if(this.image_list[i].img_id === img_id){
                this.image_list[i].img_obj = img_object;
                break
            }
        }
    },
    'fn_resize_image_in_canvas': function(img_id, canvas, MAX_WIDTH, MAX_HEIGHT){
        /**
         * resixes image in canvas
         */
        var img = this.fn_get_image_obj_by_id(img_id);
        var tempW = img.width;
        var tempH = img.height;
        if(tempW > tempH){
            if(tempW > MAX_WIDTH){
                tempH *= MAX_WIDTH / tempW;
                tempW = MAX_WIDTH;
            }
        } else{
            if(tempH > MAX_HEIGHT){
                tempW *= MAX_HEIGHT / tempH;
                tempH = MAX_HEIGHT;
            }
        }
        canvas.width = tempW;
        canvas.height = tempH;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, tempW, tempH);
    },
    'fn_update_thumbnail': function(img_id){
        /**
         * updates image thumbnail in dom object
         * @type {number}
         */
        var tw = MOBSettings.thumb_w;
        var th = MOBSettings.thumb_h;
        img = this.fn_get_image_obj_by_id(img_id);
        var canvas = document.getElementById('thumbcanvas_' + img_id);
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, tw, th);
    },
    'fn_save_image_from_canvas': function(img_id, canvas){
        /**
         * saves edited image in canvas
         */
        img_obj = this.fn_get_image_obj_by_id(img_id);
        img_obj.src = canvas.toDataURL();
        this.fn_update_image_by_id(img_id, img_obj);
        this.fn_update_thumbnail(img_id);
    },
    'fn_make_canvas_crop_editor': function(canvasid){
        /**
         * initialize crop editor's canvas
         */
        jQuery('#' + canvasid).Jcrop({
            onSelect: function(c){
                MOBImageFactory.cx = c.x;
                MOBImageFactory.cy = c.y;
                MOBImageFactory.cw = c.w;
                MOBImageFactory.ch = c.h;
            }
        });
    },
    'fn_resize_all_images': function(MAX_WIDTH, MAX_HEIGHT){
        /**
         * resizes all images smaller than given max values before upload
         * @type {number}
         */
        var img_id;
        var i;
        for(i = 0; i < this.image_list.length; i++){
            img_id = this.image_list[i].img_id;
            var canvas = document.createElement('canvas');
            this.fn_resize_image_in_canvas(img_id, canvas, MAX_WIDTH, MAX_HEIGHT);
            var img_obj = this.fn_get_image_obj_by_id(img_id);
            img_obj.src = canvas.toDataURL();
            this.fn_update_image_by_id(img_id, img_obj);
        }
        for(i = 0; i < this.image_list.length; i++){
            img_id = this.image_list[i].img_id;
            var img = this.fn_get_image_obj_by_id(img_id);
            var lg = "w:" + img.width + " h:" + img.height;
        }
    },
    'dataURLToBlob': function(dataURL){
        /**
         * create blob object from canvas's data url
         * @type {string}
         */
        var parts;
        var contentType;
        var raw;
        var BASE64_MARKER = ';base64,';
        if(dataURL.indexOf(BASE64_MARKER) === -1){
            parts = dataURL.split(',');
            contentType = parts[0].split(':')[1];
            raw = parts[1];
            return new Blob([raw], {
                type: contentType
            });
        }
        parts = dataURL.split(BASE64_MARKER);
        contentType = parts[0].split(':')[1];
        raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);
        for(var i = 0; i < rawLength; ++i){
            uInt8Array[i] = raw.charCodeAt(i);
        }
        return new Blob([uInt8Array], {
            type: contentType
        });
    },
    'fn_upload_image_in_query': function(){
        /**
         * uploads image in query
         */
        var img_id = MOBImageFactory.image_list[UPLOAD_QUERY_ITEM].img_id;
        var img = MOBImageFactory.image_list[UPLOAD_QUERY_ITEM].img_obj;
        // var img = MOBImageFactory.fn_get_image_obj_by_id(0);
        var dataUrl = img.src;
        var resizedImage = MOBImageFactory.dataURLToBlob(dataUrl);
        var data = new FormData($("form[id*='uploadImageForm']")[0]);
        var hd_ilan_id = $("#hd_ilan_id").val();
        var tag = $("#input_tag_" + img_id).val().replace(/_/g, "");
        ;
        var keystr = hd_ilan_id + "_" + tag;
        //116_resim1
        data.append(keystr, resizedImage);
        var processing_item = UPLOAD_QUERY_ITEM + 1;
        $("#div_status").html("Yukleniyor(" + processing_item + ") " + tag);
        $("#upload_progressbar").progressbar({
            value: UPLOAD_QUERY_ITEM,
            max: TOTAL_UPLOAD_ITEM
        });
        UPLOAD_QUERY_ITEM = UPLOAD_QUERY_ITEM - 1;
        $.ajax({
            url: MOBSettings.upload_url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                if(UPLOAD_QUERY_ITEM > -1){
                    MOBImageFactory.fn_upload_image_in_query();
                } else{
                    window.location.replace(MOBSettings.redirect_url);
                }
            }
        });
    }
};
var MOBEvents = {
    'fn_file_selected': function(){
        /**
         * triggered when an image file selected
         * @type {number}
         */
        var tw = MOBSettings.thumb_w;
        var th = MOBSettings.thumb_h;
        var file_dom = document.getElementById("ipt_file");
        var txt = "";
        if('files' in file_dom){
            if(file_dom.files.length !== 0){
                for(var i = 0; i < file_dom.files.length; i++){
                    img_id = MOBImageFactory.free_image_id;
                    MOBImageFactory.free_image_id += 1;
                    var file = file_dom.files[i];
                    //---------------------------
                    // ADD ITEMS TO LIST
                    //---------------------------
                    var filename = "";
                    var filesize = 0;
                    if('name' in file){
                        filename = file.name;
                    }
                    if('size' in file){
                        filesize = ((Number(file.size) / 1024) / 1024).toFixed(2);
                    }
                    var container_id = 'trfl_' + img_id;
                    var canvas_id = 'thumbcanvas_' + img_id;
                    var canvas_width = tw;
                    var canvas_height = th;
                    var btn_edit_id = 'btnedit_' + img_id;
                    var fn_edit = 'MOBEvents.fn_edit_clicked(' + img_id + ')';
                    var btn_rotate_left_id = 'btnrotl_' + img_id;
                    var fn_rotate_left = "MOBEvents.fn_rotate_single_clicked(" + img_id + ",270);";
                    var btn_rotate_right_id = 'btnrotr_' + img_id;
                    var fn_rotate_right = "MOBEvents.fn_rotate_single_clicked(" + img_id + ",90);";
                    var btn_del_id = 'btnrotr_' + img_id;
                    var fn_delete = "MOBEvents.fn_delete_clicked(" + img_id + ")";
                    var input_tag_id = "input_tag_" + img_id;
                    var cell_html = MOBSettings.cell_html;
                    cell_html = cell_html.replace(/{container_id}/g, container_id).replace(/{canvas_id}/g, canvas_id).replace(/{canvas_width}/g, canvas_width);
                    cell_html = cell_html.replace(/{canvas_height}/g, canvas_height).replace(/{btn_edit_id}/g, btn_edit_id);
                    cell_html = cell_html.replace(/{fn_edit}/g, fn_edit).replace(/{btn_rotate_left_id}/g, btn_rotate_left_id).replace(/{fn_rotate_left}/g, fn_rotate_left);
                    cell_html = cell_html.replace(/{btn_rotate_right_id}/g, btn_rotate_right_id).replace(/{fn_rotate_right}/g, fn_rotate_right);
                    cell_html = cell_html.replace(/{btn_del_id}/g, btn_del_id).replace(/{fn_delete}/g, fn_delete).replace(/{filename}/g, filename).replace(/{filesize}/g, filesize);
                    cell_html = cell_html.replace(/{input_tag_id}/g, input_tag_id);
                    $("#files_container").prepend(cell_html);
                    //---------------------------
                    // CACHE IMAGES AND CREATE THUMBNAILS
                    //---------------------------
                    var reader = new FileReader();
                    reader.img_id = img_id;
                    reader.filesize = filesize;
                    reader.onload = function(event){
                        var img = new Image();
                        img.img_id = this.img_id;
                        img.onload = function(){
                            var canvas = document.getElementById('thumbcanvas_' + this.img_id);
                            var ctx = canvas.getContext('2d');
                            ctx.drawImage(img, 0, 0, tw, th);
                        };
                        img.src = event.target.result;
                        MOBImageFactory.fn_add_image_to_list(event.target.img_id, img, event.target.filesize);
                        // var img_l = MOBImageFactory.fn_get_image_obj_by_id(img_id);
                    };
                    reader.readAsDataURL(file);
                }
            }
        }
    },
    'fn_edit_clicked': function(img_id){
        /**
         * triggered when edit button is clicked
         * @type {number}
         */
        MOBImageFactory.cx = 0;
        MOBImageFactory.cy = 0;
        MOBImageFactory.cw = 0;
        MOBImageFactory.ch = 0;
        // $("#divCanvasCropContainer").html("");
        $("#divCanvasCropContainer").html('<canvas id="canvasCrop"></canvas>');
        img = MOBImageFactory.fn_get_image_obj_by_id(img_id);
        var window_w = $(window).width() - 10;
        var window_h = $(window).height() - 10;
        var canvas = document.getElementById("canvasCrop");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, img.width, img.height);
        var dEditor = $('#dialog-editor');
        dEditor.dialog({
            height: window_h,
            width: window_w
        });
        MOBImageFactory.active_image_id = img_id;
        dEditor.dialog("open");
        MOBImageFactory.fn_make_canvas_crop_editor('canvasCrop');
    },
    'fn_delete_clicked': function(img_id){
        /**
         * triggered when delete button is clicked
         * @type {number}
         */
        MOBImageFactory.fn_delete_image_by_id(img_id);
        var selector = "#trfl_" + img_id;
        $(selector).remove();
    },
    'fn_rotate_single_clicked': function(img_id, degrees){
        /**
         * triggered when any rotate button is clicked
         * @type {number}
         */
        var canvas = document.getElementById("canvasTempWork");
        var ctx = canvas.getContext("2d");
        var image = MOBImageFactory.fn_get_image_obj_by_id(img_id);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.save();
        canvas.width = image.height;
        canvas.height = image.width;
        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.rotate(degrees * Math.PI / 180);
        ctx.drawImage(image, -image.width / 2, -image.height / 2);
        ctx.restore();
        MOBImageFactory.fn_save_image_from_canvas(img_id, canvas);
    },
    'fn_save_crop_clicked': function(){
        /**
         * triggered when save crop button clicked
         * @type {null}
         */
        img_id = MOBImageFactory.active_image_id;
        var canvas = document.getElementById("canvasCrop");
        MOBImageFactory.fn_save_image_from_canvas(img_id, canvas);
        $("#dialog-editor").dialog("close");
    },
    'fn_crop_clicked': function(){
        /**
         * triggered when crop button clicked
         * @type {HTMLImageElement}
         */
        var imageObj = new Image();
        var old_canvas = document.getElementById("canvasCrop");
        imageObj.src = old_canvas.toDataURL();
        // $("#divCanvasCropContainer").html("");
        $("#divCanvasCropContainer").html('<canvas id="canvasCrop"></canvas>');
        var canvas = document.getElementById("canvasCrop");
        canvas.width = MOBImageFactory.cw;
        canvas.height = MOBImageFactory.ch;
        var context = canvas.getContext("2d");
        if(imageObj != null && MOBImageFactory.cx !== 0 && MOBImageFactory.cy !== 0 && MOBImageFactory.cw !== 0 && MOBImageFactory.ch !== 0){
            context.drawImage(imageObj, MOBImageFactory.cx, MOBImageFactory.cy, MOBImageFactory.cw, MOBImageFactory.ch, 0, 0, canvas.width, canvas.height);
        }
        MOBImageFactory.fn_make_canvas_crop_editor('canvasCrop');
    },
    'fn_initialize_page': function(){
        /**
         * triggered at page load
         */
        // create modal window
        $("#dialog-editor").dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            open: function(event, ui){
                $(".ui-dialog-titlebar", ui.dialog | ui).hide();
            }
        });
    },
    'fn_save_all_images': function(){
        /**
         * triggered when upload button clicked
         */
        MOBImageFactory.fn_resize_all_images(MOBSettings.max_w, MOBSettings.max_h);
        $("#div_loading").show();
        UPLOAD_QUERY_ITEM = MOBImageFactory.image_list.length - 1;
        TOTAL_UPLOAD_ITEM = MOBImageFactory.image_list.length;
        // upload_progressbar
        MOBImageFactory.fn_upload_image_in_query();
    }
};
