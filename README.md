# Js Image Editor #

Js Image Editor is a simple image editing library and an example project for client side image editing and uploading.

It can **crop**, **rotate** and **bundle resize** before uploading to a server. Especially useful for e-bay like e-commerce web sites.


***


### How to use ###

You can investigate main.html as a basic usage example.

4 basic function and with  some dom objects with same id in main.html is required to implement it properly in your page.

* **MOBEvents.fn_initialize_page()** // initializes canvases etc.
* **MOBEvents.fn_file_selected()** // must be triggered when a file selected
* **MOBEvents.fn_crop_clicked()** // crop button event which shows crop work table
* **MOBEvents.fn_save_crop_clicked()** // saves cropped image
* **MOBEvents.fn_save_all_images()** // resizes all images and uploads them to the server


***


### Project structure ###

Project contains 2 main js file.

* settings.js
* imageuploader.js

#### settings.js ####
You can edit your needs in here easily 

	var MOBSettings = {    
	    'maxImageCount': // maximum image count in process, also upload file limit    
	    'thumb_w': // thumbnail width and height
	    'thumb_h':     
	    'max_w':  // maximum image size reduced before upload
	    'max_h':     
	    'redirect_url': // redirect url after upload    
	    'upload_url': // upload url
	    'cell_html': // edit window template per image
	};

### imageuploader.js ###
Contains 2 classes.

* **MOBImageFactory :** Contains image editing functions, and edited image blobs 
* **MOBEvents:** Contains event related functions